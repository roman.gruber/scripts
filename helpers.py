#!/usr/bin/env python3
"""
Some helper functions.
"""

from braceexpand import braceexpand
import re
import logging
import numpy as np
import glob as gl
import struct
from itertools import tee, islice, chain, groupby
import os
import sys
import shlex
from colorlog import ColoredFormatter
from tqdm import tqdm
import matplotlib.pyplot as plt
from uncertainties import ufloat, unumpy
import inspect
import hashlib
import json
import time
import atexit
import math
from funcy import walk_values


logger = logging.getLogger("utils")


def print_runtime():
    end_time = time.time()
    elapsed_time = end_time - start_time
    logger.debug(f"Script runtime: {elapsed_time:.2f} seconds")

start_time = time.time()
atexit.register(print_runtime)


def error(test, msg, ErrorType=ValueError):
    if test:
        logger.critical(msg)
        raise ErrorType(msg)

def braced_glob(path, glob=True):
    lst = []
    for x in braceexpand(path):
        lst.extend(gl.glob(x) if glob else [x])
    return lst

def group_by_configs(infiles, regex=r'\/([^\/]+)n(?=([0-9]+)_)', invasive=True):
    dic = {}
    for infile in infiles:
        if (m := re.search(regex, infile)):
            run_name = m.group(1)
            cnfg = int(m.group(2))
            if cnfg not in dic:
                dic[cnfg] = []
            dic[cnfg].append(infile)

    logger.debug(f"added configs {dic.keys()} to dict")
    lens = np.array([len(v) for v in dic.values()])
    if invasive and not np.all(lens == lens[0]):
        logger.error('Error: not all configs have the same amount of sources')
        exit()

    return dict(sorted(dic.items())), lens[0], len(dic.keys()), run_name

def read_coords(infile):
    int_size = 4 # 4 bytes
    with open(infile, "rb") as f:
        N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
        N1 = struct.unpack('<1I', f.read(int_size))[0]
        N2 = struct.unpack('<1I', f.read(int_size))[0]
        N3 = struct.unpack('<1I', f.read(int_size))[0]
        return N0, N1, N2, N3

def read_dat(infile):
    int_size = 4 # 4 bytes
    double_size = 8 # 8 bytes IEEE binary64 float
    with open(infile, "rb") as f:
        N0 = struct.unpack('<1I', f.read(int_size))[0] # 1 unsigned int I, < little endian
        N1 = struct.unpack('<1I', f.read(int_size))[0]
        N2 = struct.unpack('<1I', f.read(int_size))[0]
        N3 = struct.unpack('<1I', f.read(int_size))[0]
        data = struct.unpack('<{0}d'.format(2*N0), f.read(double_size*2*N0)) # 2*N0 doubles
        G = np.zeros(N0, dtype=np.complex128)
        G.real = np.array(data[0:2*N0:2]) # only even indices, real part
        G.imag = np.array(data[1:2*N0+1:2])  # only odd indices, imag part
        return G

def write_dat(G, N, outfile):
    int_size = 4 # 4 bytes
    double_size = 8 # 8 bytes IEEE binary64 float
    with open(outfile, "wb") as f:
        f.write(struct.pack('<1I', N[0]))
        f.write(struct.pack('<1I', N[1]))
        f.write(struct.pack('<1I', N[2]))
        f.write(struct.pack('<1I', N[3]))
        f.write(struct.pack('<{0}d'.format(2*N[0]), *G.view(np.float64)))

def get_logger(name="utils"):
    return logging.getLogger(name)

def setup_logger(verbosity=logging.DEBUG, fmt='[%(funcName)s] %(log_color)s%(message)s%(reset)s', formatter=ColoredFormatter):
    logger.setLevel(logging.DEBUG if verbosity else logging.INFO)
    ch = logging.StreamHandler()
    ch.setFormatter(formatter(fmt))
    logger.addHandler(ch) # add handlers to logger
    return logger

def previous_and_next(some_iterable):
    """
    Yield previous, current, next of an iterable

    :param      some_iterable:  Some iterable
    :type       some_iterable:  Iterable

    :returns:   Iterable with tuple(previous, current, next)
    :rtype:     Iterable
    """
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return zip(prevs, items, nexts)

def infile_list(infiles):
    lst = []
    for _, element, nxt in previous_and_next(infiles):
        if os.path.isfile(element):
            lst.append(element)
        if os.path.isdir(element):
            if nxt is not None:
                l = braced_glob(f"{element}/{nxt}")
                if (len(l) == 0):
                    logger.error(f"No files matching {element}/{nxt}")
                    exit()
                logger.debug(f"adding {len(l)} files from directory {element} using glob {nxt}")
                lst.extend(l)
            else:
                raise argparse.ArgumentTypeError(f"{nxt} is not a shell glob")
    return sorted(lst)


def str_insert(string, idx, substring):
    return string[:idx] + substring + string[idx:]

def pretty_cmd(split=[r" \-[\-0-9a-zA-Z]+ "], min_length=40, insert=' \\\n    '):
    """
    Prettify command by adding newlines

    :param      split:       Where to add newlines
    :type       split:       Array
    :param      min_length:  The minimum length of a chunk
    :type       min_length:  int
    :param      insert:      What string to insert at split index
    :type       insert:      str

    :returns:   Prettified command line
    :rtype:     str
    """
    cmd = shlex.join(sys.argv)
    ptr, offset = 0, 0
    for sp in split:
        for idx in [0] + [m.start() for m in re.compile(sp).finditer(cmd)]:
            if idx-ptr > min_length:
                cmd = str_insert(cmd, idx+offset, insert)
                ptr = idx
                offset += len(insert)

    return cmd


def plt_metadata():
    return {
        'Keywords': pretty_cmd(),
        'Creator': os.getcwd(),
    }

def plt_finalise(fig, outfile = None):
    """
    Finalise a plot

    :param      fig:      The figure object
    :type       fig:      object
    :param      outfile:  The output file
    :type       outfile:  string
    """
    if outfile is None:
        logger.debug(f"Showing plot")
        plt.draw()
        plt.show()
    else:
        logger.debug(f"Storing {outfile}")
        plt.savefig(outfile, dpi=fig.dpi, transparent=True, metadata=plt_metadata())


from matplotlib.backends.backend_pdf import PdfPages
class Pdf(PdfPages):
    def __init__(self, filename, *args, **kwargs):
        kwargs.setdefault("metadata", plt_metadata())
        self._filename = filename
        super().__init__(filename, *args, **kwargs)

    def __exit__(self, *args, **kwargs):
        if self._filename is None:
            logger.debug(f"Showing plot")
            plt.draw()
            plt.show()
        else:
            logger.debug(f"Storing {self._filename}")
            super().__exit__(*args, **kwargs)

    def savefig(self, *args, **kwargs):
        kwargs.setdefault("transparent", True)
        return super().savefig(*args, **kwargs)


def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)


def group_infiles(infiles, matcher=None, exclude={}, limit_nconfigs=None):
    default_matcher = {
        "regex": r'n(?=([0-9]+)_).*_t([0-9]+)_s([0-9]+)',
        "main_group": {"name": "nconfig", "regex": r'n(?=([0-9]+)_)', "group": 1},
        "groups": { # group can be a tuple
            "ntime":   {"regex": r'_t([0-9]+)_s',                 "group": 1},
            "nsrc":    {"regex": r'\/([^\/]+)\/[^\/]+_s([0-9]+)', "group": (1,2)}, # distict sources are in distict dirs, even when they have the same index _sN_
        },
    }
    if matcher is None: matcher = default_matcher

    # get matching group even if it's a tuple
    get_group = lambda m, grp: m.groups(grp) if isinstance(grp, tuple) else m.group(grp)

    def get_main_group(matcher, infile):
        mg = matcher.get("main_group")
        error(not (m := re.search(mg.get("regex"), infile)), f"File {infile} does not match regex {mg.get('regex')}")
        return get_group(m, mg.get("group"))

    mg_name = matcher.get("main_group").get("name")

    N0, _, _, _ = read_coords(infiles[0])
    _infiles = []
    mapping = {}
    for infile in infiles:
        mg = get_main_group(matcher, infile)
        skip = mg_name in exclude and mg in map(str, exclude[mg_name])

        for (name, v) in matcher.get("groups").items():
            error(not (m := re.search(v.get("regex"), infile)), f"File {infile} does not match regex {v.get('regex')}")
            extracted = get_group(m, v.get("group"))
            if not skip:
                mapping.setdefault(mg, {})
                mapping[mg].setdefault(name, set())
                mapping[mg][name].add(extracted)

        if not skip:
            _infiles.append(infile)

    configs = sorted(list([k for k in mapping.keys()]))
    mapping = walk_values(lambda d: walk_values(list, d), mapping)
    mapping = walk_values(lambda d: walk_values(sorted, d), mapping)
    lengths = {name: [len(value[name]) for config, value in mapping.items()] for name in matcher.get("groups").keys()}

    for name, length in lengths.items():
        error(not all_equal(length), f"There are configs with different number of {name} ({length}).")

    lens = {**{"nconfigs": len(mapping.keys())}, **{name: length[0] for name, length in lengths.items()}, **{"N0": N0}}
    dims = tuple(lens.values())
    dim = np.prod(dims[:-1])

    if limit_nconfigs is not None:
        exclude["nconfig"] = list(set(configs) - set(configs[0:limit_nconfigs]))
        return group_infiles(infiles, matcher=matcher, exclude=exclude, limit_nconfigs=None)

    error(dim != len(_infiles), f"The number of dictict files {len(_infiles)} should equal the array dimensions {dim} {dims[:-1]}")

    G = np.zeros(dims, dtype=np.dtype(np.complex128, metadata=lens))
    logger.debug(f"Reading {dim} files, {lens}, configs: {configs}")
    indices = set()
    for infile in tqdm(_infiles, total=len(_infiles)):
        match = {}
        mg = get_main_group(matcher, infile)
        match[mg_name] = mg
        for (name, v) in matcher.get("groups").items():
            m = re.search(v.get("regex"), infile)
            match[name] = get_group(m, v.get("group"))
        idx = (configs.index(mg), ) + tuple([mapping[mg][name].index(match[name]) for name in matcher.get("groups").keys()])
        indices.add(idx)
        G[idx] = read_dat(infile)

    error(len(indices) != len(_infiles), f"Not all indices are unique")

    return G


def emap(f, lst, evaluate=True, mode="normal", **kwargs):
    """
    Like map, but evaluates to a list

    :param      f:         Function to evaluate on every element of the iterable
    :type       f:         callable
    :param      lst:       The list or iterable
    :type       lst:       iterable
    :param      evaluate:  Whether to evaluate or not (default=True)
    :type       evaluate:  bool
    :param      mode:      The mode (can be normal, star or starstar)
    :type       mode:      str
    :param      kwargs:    The keywords arguments for the function f()
    :type       kwargs:    dictionary

    :returns:   Evaluated list or lacy evaluated iterator
    :rtype:     iterator
    """
    if mode=="normal":
        res = map(lambda x: f(x, **kwargs), lst)
    elif mode=="star":
        res = map(lambda x: f(*x, **kwargs), lst)
    elif mode=="starstar":
        res = map(lambda x: f(**x, **kwargs), lst)
    return list(res) if evaluate else res


def show_shape(n, G, info=None, name="G", dims=None):
    """
    Log the shape of an iterable.

    :param      n:     name of the element
    :type       n:     int/string
    :param      G:     Correlator dict
    :type       G:     dictionary
    :param      info:  Additional information to be printed
    :type       info:  string
    :param      name:  Name of the variable G
    :type       name:  str
    :param      dims:  The names of the dimensions
    :type       dims:  string
    """
    dtype = type(G.flat[0]).__name__
    if dims is None:
        dims = None if G.dtype.metadata is None else ", ".join(tuple(G.dtype.metadata.keys()))
    string = f"{name}"
    if n is not None: string += f"[{n}]"
    string += f".shape = {G.shape}\t(leaf dtype={dtype})\t({dims})\t({info})"
    logger.debug(string)


# Fold the deepest dimension of a numpy array
def fold(G):
    N0 = G.shape[-1]
    r1 = np.arange(0, int(N0/2))
    r2 = np.flip(np.concatenate((np.arange(int(N0/2)+1, N0), [0])))
    return np.mean([G[...,r1], G[...,r2]], axis=0)


from cycler import cycler
prop_cycle = plt.rcParams['axes.prop_cycle']
color_cycle = prop_cycle.by_key()['color']
marker_cycle = ['x', 'o', '+', 'D', 's', '*', '^', 'v', '<', '>']
default_cycler = (cycler(color=color_cycle) + cycler(marker=marker_cycle))
plt.rc('axes', prop_cycle=default_cycler)


def plot(ax, Y, X = None, shift=0.0, **kwargs):
    """
    Custom plot function that can deal with ufloats

    :param      ax:      Axis object to plot to
    :type       ax:      Axes instance
    :param      Y:       Possibly a unumpy array of y-values
    :type       Y:       numpy.array / unumpy.array
    :param      X:       Values for the x-axis or None
    :type       X:       numpy.array / None
    :param      kwargs:  The keywords arguments for errorbar
    :type       kwargs:  dictionary

    :returns:   See matplotlib errorbar()
    :rtype:     ErrorbarContainer
    """

    # some default plot arguments
    opts = {
        #'xerr': 0,
        #'yerr': Y_errs,
        'capsize': 2,
        'alpha': 0.5,
        'linewidth': 1,
        'fillstyle': 'none',
    }

    Y_vals = unumpy.nominal_values(np.real(Y))
    Y_errs = unumpy.std_devs(np.real(Y))
    if np.linalg.norm(Y_errs) != 0.0:
        opts['yerr'] = Y_errs

    if X is None:
        X_vals = np.arange(Y.shape[0]) + plot.shift*(-1.0)**plot.counter
        X_errs = [0]
    else:
        X_vals = unumpy.nominal_values(np.real(X))
        X_errs = unumpy.std_devs(np.real(X))
        if np.linalg.norm(X_errs) != 0.0:
            opts['xerr'] = X_errs

    opts = dict(list(opts.items()) + list(kwargs.items())) # kwargs override opts

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    #print(f"{colors = }")
    #print(f"{prop_cycle.keys = }")
    #exit()

    plot.shift += shift
    plot.counter += 1
    return ax.errorbar(X_vals, Y_vals, **opts)
plot.shift = 0.0
plot.counter = 0


def save(filename, array, **kwargs):
    """
    Store a unumpy.uarray into a file.

    :param      filename:  The filename
    :type       filename:  str
    :param      array:     The array
    :type       array:     unumpy.uarray
    :param      kwargs:    The keywords arguments for np.save
    :type       kwargs:    dictionary
    """
    with open(filename, 'wb') as f:
        logger.debug(f"Storing array to {filename}")
        np.save(f, unumpy.nominal_values(array), **kwargs)
        np.save(f, unumpy.std_devs(array), **kwargs)


def load(filename, **kwargs):
    """
    Load a unumpy.uarray from a file.

    :param      filename:  The filename
    :type       filename:  str
    :param      kwargs:    The keywords arguments for np.load
    :type       kwargs:    dictionary

    :returns:   The array
    :rtype:     unumpy.uarray
    """
    with open(filename, 'rb') as f:
        logger.debug(f"Loading array from {filename}")
        return unumpy.uarray(np.load(f, **kwargs), np.load(f, **kwargs))


def cache_to_disk(arguments, path='/tmp/helpers.py'):
    """
    Decorator to cache return values to disk,

    :param      arguments:  The arguments and their hashing function to respect
                            for caching
    :type       arguments:  dict
    :param      path:       The path to store the cached return values
    :type       path:       str

    :returns:   Decorator
    :rtype:     callable
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            keys = inspect.getfullargspec(func).args
            vals = [v for v in chain(args, kwargs.values())]
            all_params = dict(zip(keys, vals))
            params = {key:f(all_params[key]) for key,f in arguments.items()}
            md5 = hashlib.md5(json.dumps(params, sort_keys=True).encode('utf-8')).hexdigest()
            fname = f"{path}/{md5}.dat"
            if os.path.isfile(fname):
                logger.debug(f"Loading cached entry for {func.__name__} from file {fname}")
                ret = load(fname)
            else:
                ret = func(*args, **kwargs)
                logger.debug(f"Storing entry for {func.__name__} in file {fname}")
                save(fname, ret)
            return ret
        return wrapper
    return decorator


def hash_numpy_array(arr):
    """
    Hash a numpy array

    :param      arr:  The array
    :type       arr:  numpy.ndarray

    :returns:   MD5 hash
    :rtype:     str
    """
    return hashlib.md5(arr.tobytes() + str.encode(str(arr.shape))).hexdigest()


def dump_args(args):
    logger.debug("Running with the following arguments:")
    logger.debug(json.dumps(vars(args), indent=4))


def fix_xlim(ax, x_min, x_max, scale='log', base=2):
    """
    Set the limit of the x-axis including a margin of the autoscaler.

    :param      ax:     The axis object
    :type       ax:     Axes
    :param      x_min:  The x minimum
    :type       x_min:  float
    :param      x_max:  The x maximum
    :type       x_max:  float
    :param      scale:  The scale (currently supported 'linear' and 'log')
    :type       scale:  str
    :param      base:   The base for the log scale
    :type       base:   int
    """
    margin, _ = ax.margins() # default 0.05, see https://matplotlib.org/stable/users/explain/axes/autoscale.html
    if scale == 'linear':
        transformA = lambda x: x
        transformB = lambda x: x
    elif scale == 'log':
        transformA = lambda x: math.log(x, base)
        transformB = lambda x: base**x
    else:
        hp.error(True, f"Unknown scale {scale}")

    span = abs(transformA(x_max) - transformA(x_min))
    mn = transformB(transformA(x_min) - margin*span)
    mx = transformB(transformA(x_max) + margin*span)
    ax.set_xlim([mn, mx])


def unique_list(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def compactify_legend(ax, **kwargs):
    """
    Compactify a legend where 2 artists have the same label

    :param      ax:   The axis
    :type       ax:   axis
    """
    handles, labels = ax.get_legend_handles_labels()
    new_handles = [tuple([handles[i] for i, value in enumerate(labels) if value == label]) for label in labels]
    ax.legend(unique_list(new_handles), unique_list(labels), **kwargs)
