#!/usr/bin/env python3
"""
Edit correlators.
"""

import argparse
import json
from tqdm import tqdm
import helpers as hp
import difflib
import os
from itertools import cycle
from functools import reduce


logger = hp.get_logger()

def error(test, msg, ErrorType=ValueError):
    if test:
        logger.critical(msg)
        raise ErrorType(msg)


def create_fname(file1, file2, op="+", outname=["({p1}{op}{p2})"], max_diffs=1, max_diff_chars=1, same_lengths=True):
    """
    Creates a new merged filename from two filename.

    :param      file1:        The filename 1 (only the basename!)
    :type       file1:        str
    :param      file2:        The filename 2 (only the basename!)
    :type       file2:        str
    :param      op:           The operation performed on the data of the two
                              files
    :type       op:           str
    :param      outname:      The output filename patterns for each difference of the
                              two input filenames (cycles if end is reached)
    :type       outname:      str

    :returns:   The new merged filename
    :rtype:     str
    """
    s = difflib.SequenceMatcher(None, file1, file2)
    same_seg_lengths = reduce(lambda a, b: a and b, map(lambda m: m.a==m.b, s.get_matching_blocks()))

    diffs = len(s.get_matching_blocks())-2 # first and last block don't count
    result = ""
    diff_chars = min(len(file1), len(file2))
    oname = cycle(outname)
    for pre, curr, _ in hp.previous_and_next(s.get_matching_blocks()[:-1]):
        diff_chars -= curr.size
        if (pre is not None):
            result += next(oname).format(**{
                "op": op,
                "p1": file1[pre.a+pre.size:curr.a],
                "p2": file2[pre.b+pre.size:curr.b],
            })
        result += file1[curr.a:curr.a+curr.size]

    error(same_lengths and not same_seg_lengths, f"Not all segments have the same lengths ({s.get_matching_blocks() = }):\n{file1 = }\n{file2 = }\n{result = }")
    error(diffs > max_diffs, f"Number of differences exceeded ({diffs = }, {max_diffs = }):\n{file1 = }\n{file2 = }\n{result = }")
    error(diff_chars > max_diff_chars, f"Number of different characters exceeded ({diff_chars = }, {max_diff_chars = }):\n{file1 = }\n{file2 = }\n{result = }")
    return result

def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
    parser.add_argument('--op', help="operator to apply (default +)", default="+", type=str, choices=['+', '-'],)
    parser.add_argument('-o', '--outname', help='replacement for output file name(s) (Example: if outname="ABC", then n1f3.dat + n2f4.dat -> nABCfABC.dat, if outname=None, then n1f3.dat + n2f4.dat -> n(1+2)f(3+4).dat).', type=str, nargs='+', default=["({p1}{op}{p2})"])
    parser.add_argument('-d', '--dry-run', help="don't do anything, just show (use -v to see what would happen)", default=False, action="store_true")
    parser.add_argument('-f', '--force', help="force overwriting existing file(s) (default: don't overwrite)", default=False, action="store_true")
    parser.add_argument('-l', '--lengths-may-differ', help="don't enforce pairs of -i1 and -i2 to have the same filename length (default: enforce)", default=False, action="store_true")
    parser.add_argument('-md', '--max-diffs', help="maximal number of differences in the two filenames (Example: n1f3.dat + n2f4.dat have two differences)", type=int, default=1)
    parser.add_argument('-ms', '--max-diff-chars', help="maximal number characters that differ in the two filenames (Example: n1f45.dat + n2f67.dat have 3 characters difference, n1f45.dat + n2f47.dat have 2 characters difference)", type=int, default=1)

    required = parser.add_argument_group('required named arguments')
    required.add_argument('-i1', '--infile1', help='input file(s).', metavar='FILE', required=True, nargs='+')
    required.add_argument('-i2', '--infile2', help='input file(s).', metavar='FILE', required=True, nargs='+')
    required.add_argument('--outdir', help='directory for output file(s).', metavar='DIR', required=True)

    args = parser.parse_args()
    fmt = '[%(funcName)s] %(log_color)s%(message)s%(reset)s'
    logger = hp.setup_logger(args.verbosity, fmt="Dry-run " + fmt if args.dry_run else fmt)
    logger.debug(f"Running with the following arguments:\n{json.dumps(vars(args), indent=4)}")

    operators = {
        "+": ("addition", lambda a, b: a+b),
        "-": ("subtraction", lambda a, b: a-b),
    }
    infiles1 = hp.infile_list(args.infile1)
    infiles2 = hp.infile_list(args.infile2)
    l1, l2 = len(infiles1), len(infiles2)
    error(l1 != l2, f"-i1 and -i2 need to have the same amount of files ({l1 = }, {l2 = })")

    for n, (file1, file2) in enumerate(tqdm(zip(infiles1, infiles2), desc=f"processing dat-files"), start=1):
        N1 = hp.read_coords(file1)
        N2 = hp.read_coords(file2)
        error(N1 != N2, f"Lattice dimensions do not coincide ({N1 = }, {N2 = })")

        bn_file1, bn_file2 = os.path.basename(file1), os.path.basename(file2)
        outfile = create_fname(bn_file1, bn_file2, op=args.op, outname=args.outname,
            max_diffs=args.max_diffs, max_diff_chars=args.max_diff_chars,
            same_lengths=not args.lengths_may_differ)
        outfile = os.path.abspath(f"{args.outdir}/{outfile}")
        op_str, op_func = operators[args.op]

        logger.debug(f"Creating file as {op_str} from files (#{n}/{l1}):\n * {file1}\n {args.op} {file2}\n = {outfile}")
        error(not args.force and os.path.isfile(outfile), f"File {outfile} already exists")
        error(not args.lengths_may_differ and len(bn_file1) != len(bn_file2), f"Filenames don't have the same length ({len(file1) = }, {len(file2) = }, {bn_file1 = }, {bn_file2 = })")

        if os.path.isfile(outfile) and args.force:
            logger.warning(f"Overwriting file {outfile}")

        if not args.dry_run:
            hp.write_dat(op_func(hp.read_dat(file1), hp.read_dat(file2)), N1, outfile)

if __name__ == '__main__':
    main()
