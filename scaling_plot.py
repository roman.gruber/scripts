#! /usr/bin/env python3
#
# Don't forget to:
# module load cray-python
#


import os
import re
import sys
import math
from pathlib import Path
from glob import iglob
import numpy as np
import matplotlib.pyplot as plt
import helpers as hp
from types import SimpleNamespace
from schema import Schema, And, Or, Use, Optional, SchemaError, Const
from functools import reduce
from datetime import datetime
from matplotlib.backends.backend_pdf import PdfPages
from uncertainties import ufloat, unumpy
import helpers as hp
import argparse


logger = hp.get_logger()

nproc = lambda r: extract(r.file, r'(\d+)x(\d+)x(\d+)x(\d+) process grid', (1,2,3,4), lambda x: np.prod(list(map(int, x))))
lvol = lambda r: extract(r.file, r'(\d+)x(\d+)x(\d+)x(\d+) lattice,', (1,2,3,4), lambda x: np.prod(list(map(int, x))))

def nproc_divided_by(ranks_per_node):
  return lambda r: math.ceil(nproc(r)/ranks_per_node)

cred = (255/255, 64/255, 0/255)
cblue = (64/255, 128/255, 255/255)

def ignore_first(x):
  return np.mean(x[1:])

plots = [
  {
    "active": False,
    "filename": "~/plots/scaling/F7_coarse_scaling_Nb.pdf",
    "name": "F7",
    "scaling": 2,
    "properties": {"xlabel": "Nb (Number of blocks)"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=25", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns50_B*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=50", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns100_B*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=100", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns200_B*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=200", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns400_B*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=256", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns512_B*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
    ],
  },
  {
    "active": True,
    "filename": "/users/groman/plots/todi_inv_strong.pdf",
    "name": "CSCS Alps testing system (GH200)",
    "properties": {"xlabel": "GPUs"},
    "lines": [
      {
        "plot_args": {"label": "C380: GDR=off, NVSHMEM=off"},
        "glob": f"/capstor/scratch/cscs/groman/log/quda/gpu/todi/C380/test/*_GDR0_NVSHMEM0/*check3*.log", # where are the log files?
        "x": nproc_divided_by(1),
        "time": lambda r: extract(r.file, r'QUDA \(rank=0\):   secs        = ([\d\.e\+\-]+)', 1, reduction=lambda x: x[1:]),
        "extract": {
          "iteration count": lambda r: extract(r.file, r'QUDA \(rank=0\):   iter        = (\d+)', 1, reduction=lambda x: x),
          #"memory usage": lambda r: extract(r.file, r'QUDA \(rank\=0\)\: Device memory used = ([\d\.e\+\-]+) MiB', 1),
          #"tuning": lambda r: extract(r.file, r'Tuned block', 0, mapping = str, reduction = lambda x: x.size),
        },
      },
      {
        "plot_args": {"label": "G8: GDR=off, NVSHMEM=off"},
        "glob": f"/capstor/scratch/cscs/groman/log/quda/gpu/todi/G8/test/*_GDR0_NVSHMEM0/*check3*.log", # where are the log files?
        "x": nproc_divided_by(1),
        "time": lambda r: extract(r.file, r'QUDA \(rank=0\):   secs        = ([\d\.e\+\-]+)', 1, reduction=lambda x: x[1:]),
        "extract": {
          "iteration count": lambda r: extract(r.file, r'QUDA \(rank=0\):   iter        = (\d+)', 1, reduction=lambda x: x),
          #"memory usage": lambda r: extract(r.file, r'QUDA \(rank\=0\)\: Device memory used = ([\d\.e\+\-]+) MiB', 1),
          #"tuning": lambda r: extract(r.file, r'Tuned block', 0, mapping = str, reduction = lambda x: x.size),
        },
      },
    ]
  },
  {
    "active": False,
    "filename": "~/plots/scaling/F7_coarse_scaling_Ns.pdf",
    "name": "F7",
    "scaling": 2,
    "properties": {"xlabel": "Ns (Number of eigenmodes)"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=41k, bs4x4x4x4", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns*_B4x4x4x4/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=8k, bs6x6x6x6", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns*_B6x6x6x6/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=2.6k, bs8x8x8x8", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns*_B8x8x8x8/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "F7 #ranks": lambda r: nproc(r),
          "F7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
    ]
  },
  {
    "active": False,
    "filename": "~/plots/scaling/E7_coarse_scaling_Nb.pdf",
    "name": "E7",
    "scaling": 2,
    "properties": {"xlabel": "Nb (Number of blocks)"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=25", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/dat/panel/Ns50_type1_bs*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "E7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "E7 #ranks": lambda r: nproc(r),
          "E7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=50", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/dat/panel/Ns100_type1_bs*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "E7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "E7 #ranks": lambda r: nproc(r),
          "E7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=100", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/dat/panel/Ns200_type1_bs*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "E7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "E7 #ranks": lambda r: nproc(r),
          "E7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
    ]
  },
  {
    "active": False,
    "filename": "~/plots/scaling/E7_coarse_scaling_Ns.pdf",
    "name": "E7",
    "scaling": 2,
    "properties": {"xlabel": "Ns (Number of eigenmodes)"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=8k, bs4x4x4x4", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/dat/panel/Ns*_type1_bs4x4x4x4/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "E7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "E7 #ranks": lambda r: nproc(r),
          "E7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=512, bs8x8x8x8", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/dat/panel/Ns*_type1_bs8x8x8x8/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "E7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "E7 #ranks": lambda r: nproc(r),
          "E7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
    ]
  },
  {
    "active": False,
    "filename": "~/plots/scaling/G7_coarse_scaling_Nb.pdf",
    "name": "G7",
    "scaling": 2,
    "properties": {"xlabel": "Nb (Number of blocks)"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=25", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns50_type1_bs*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "G7 #ranks": lambda r: nproc(r),
          "G7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=50", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns100_type1_bs*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "G7 #ranks": lambda r: nproc(r),
          "G7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Ns=100", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns200_type1_bs*/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\[lma_new\] lma.*.nb = ([\d]+)', 1)*nproc(r),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "G7 #ranks": lambda r: nproc(r),
          "G7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
    ]
  },
  {
    "active": False,
    "filename": "~/plots/scaling/G7_coarse_scaling_Ns.pdf",
    "name": "G7",
    "scaling": 2,
    "properties": {"xlabel": "Ns (Number of eigenmodes)"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=131k, bs4x4x4x4", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns*_type1_bs4x4x4x4/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "G7 #ranks": lambda r: nproc(r),
          "G7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=8k, bs8x8x8x8", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns*_type1_bs8x8x8x8/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "G7 #ranks": lambda r: nproc(r),
          "G7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "Nb=512, bs16x16x16x16", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns*_type1_bs16x16x16x16/*.log", # where are the log files?
        "x": lambda r: extract(r.file, r'\(Ns_orig = ([\d]+)\)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
          "G7 #ranks": lambda r: nproc(r),
          "G7 time/point": lambda r: r.time(r)/extract(r.file, r'\[lma_new\] lma.*.dim = ([\d]+)', 1),
        },
      },
    ]
  },

  {
    "active": False,
    "filename": "~/plots/scaling/iterations.pdf",
    "name": "Iterations",
    "scaling": 2,
    "properties": {"xlabel": "ratio dim(space)/dim(subspace)", "xscale": "log"},
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "G7", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/dat/panel/Ns*_type1_*/*.log", # where are the log files?
        "x": lambda r: (12*lvol(r))/extract(r.file, r'\[lma_new\] lma.*dim = ([\d]+)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "G7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "F7", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/3rd_run/Ns*_B*/*.log", # where are the log files?
        "x": lambda r: (12*lvol(r))/extract(r.file, r'\[lma_new\] lma.*dim = ([\d]+)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "F7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
        },
      },
      {
        "ideal_speedup": False,
        "plot_args": {"label": "E7", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/dat/panel/Ns*_type1_*/*.log", # where are the log files?
        "x": lambda r: (12*lvol(r))/extract(r.file, r'\[lma_new\] lma.*dim = ([\d]+)', 1),
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "E7 #iterations": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
        },
      },
    ]
  },

  {
    "active": True,
    "filename": "~/plots/scaling/test.pdf",
    "name": "test",
    "scaling": 2,
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "E7", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/E7/final/str2/*.log", # where are the log files?
        "x": lambda r: 1,
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "L0 avg": lambda r: extract(r.file, r'stats\.avg_time\s+= ([\d\.e\+\-]+) sec', 1),
          "L0 iter": lambda r: extract(r.file, r'stats\.avg_iter\s+= \[ ([\d\.e\+\-]+), ', 1),
          "L1 iter": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
        },
      },
    ]
  },

  {
    "active": True,
    "filename": "~/plots/scaling/test.pdf",
    "name": "test",
    "scaling": 2,
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "F7", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/F7/final/str2/*.log", # where are the log files?
        "x": lambda r: 1,
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "L0 avg": lambda r: extract(r.file, r'stats\.avg_time\s+= ([\d\.e\+\-]+) sec', 1),
          "L0 iter": lambda r: extract(r.file, r'stats\.avg_iter\s+= \[ ([\d\.e\+\-]+), ', 1),
          "L1 iter": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
        },
      },
    ]
  },

  {
    "active": True,
    "filename": "~/plots/scaling/test.pdf",
    "name": "test",
    "scaling": 2,
    "lines": [
      {
        "ideal_speedup": False,
        "plot_args": {"label": "G7", "linestyle": "-"},
        "glob": f"/home/hpcp/rgruber/G7/final/str2/*.log", # where are the log files?
        "x": lambda r: 1,
        "time": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.time\.avg\s+= ([\d\.e\+\-]+) sec', 1),
        "extract": {
          "L0 avg": lambda r: extract(r.file, r'stats\.avg_time\s+= ([\d\.e\+\-]+) sec', 1),
          "L0 iter": lambda r: extract(r.file, r'stats\.avg_iter\s+= \[ ([\d\.e\+\-]+), ', 1),
          "L1 iter": lambda r: extract(r.file, r'lma\.stats\.Ainvop\.status\.avg\s+= ([\d\.e\+\-]+) ', 1),
        },
      },
    ]
  },

]

# overall plot arguments
plot_args = {
  "speedup": { # speedup data lines
    "fmt": "x",
    "markeredgewidth": 1,
    "capsize": 3,
    "mfc": "none",
    "linestyle": "--",
  },
  "tts": { # TTS data lines
    "fmt": "x",
    "markeredgewidth": 1,
    "capsize": 3,
    "mfc": "none",
    "linestyle": "",
  },
  "ideal": { # ideal scaling lines
    "linewidth": 1.0,
    "linestyle": "--",
    "fmt": "",
    "color": "black",
    "label" :"ideal scaling",
  },
  "other": { # other lines
    "fmt": "x",
    "markeredgewidth": 1,
    "capsize": 3,
    "mfc": "none",
    "linestyle": ":",
  },
}

def umean(x):
  return ufloat(np.mean(x), np.std(x))

# schema to validate input list above and convert to a SimpleNamespace
ns = lambda x: SimpleNamespace(**x)
schema = Schema([And({
  Optional("active", default=False): bool,    # generate plot for these
  "filename": And(Use(os.path.expanduser), len),                  # path to output plot filename
  "name": And(str, len),                      # name of the plot
  Optional("scaling", default=0): And(int, lambda x: x==0 or x==1 or x==2), # strong=0, weak=1, none=2
  Optional("properties", default={}): dict,   # additional kwargs for the plot (axis label, ...)
  "lines": [And({
    Optional("plot_args", default={}): {str: lambda x: x}, # additional kwargs for lines (colors, styles, ...)
    "glob": lambda x: len(list(iglob(x)))>0,  # glob to match log files to include
    "x": callable,    # x axis: number of devices/ranks/nodes/GPUs
    Optional("reduce", default=None): callable,
    "time": callable, # y axis: TTS in seconds
    Optional("extract", default={}): {str: callable}, # additional metrics to extract
    Optional("ideal_speedup", default=True): bool,  # add an ideal speedup line in the TTS plot
    Optional("restrict", default=np.s_[:]): object, # restrict extracted data (numpy slice)
  }, Use(ns))]
}, Use(ns))])

def file_contains(file, string):
  with open(file, "r", encoding='utf-8', errors='ignore') as fp:
    for line in fp:
      if string in line:
        return True
  return False

class ExtractException(Exception):
    pass

def extract(file, pattern, group, mapping = np.float64, reduction = np.mean, verbose=False):
  """
  Extract data from a log file

  :param      file:       The log file
  :type       file:       string
  :param      pattern:    regex pattern to search the file
  :type       pattern:    string
  :param      group:      Pattern matching group to extract
  :type       group:      int or tuple of ints
  :param      mapping:    Mapping function to postprocess matches
  :type       mapping:    callable
  :param      reduction:  Reduction function if multiple matches found
  :type       reduction:  callable

  :returns:   Reduced mapped match
  :rtype:     mixed

  :raises     Exception:  If match not found
  """
  ret = []
  r = re.compile(pattern)
  with open(file, "r", encoding='utf-8', errors='ignore') as fp:
    lines = '\n'.join(fp.readlines())
    for result in r.finditer(lines):
      if type(group)==tuple:
        ret.append(mapping(result.group(*group)))
      else:
        ret.append(mapping(result.group(group)))

  if not ret:
    raise ExtractException("Unable to extract something")
  if verbose:
    logger.debug(f"\nfrom {file}:")
    logger.debug(f"  extracted: {ret} using \"{pattern}\"")
    logger.debug(f"  reduced: {reduction(np.array(ret))} using {reduction.__name__}()")
  return reduction(np.array(ret))

def get_speedup(Y, base_idx=0):
  return np.array([Y[base_idx]/y for y in Y])

def try_extract(key, f, run):
  x = None
  success = False
  try:
    x = f(run)
    success = True
  except ExtractException:
    logger.warning(f"Nothing found for {key} in {run.file}")
  except:
    raise
  return x, success

def get_X(iterator, mn=None, mx=None):
  mn = int(np.log2(min(iterator))) if mn is None else mn
  mx = int(np.log2(max(iterator))) if mx is None else mx
  return np.array([2**n for n in range(mn, mx+1)], dtype=int)

def merge_by_x(X, Y, reduction=None, dtype=np.float64):
  r = umean if reduction is None else reduction
  X_vals, idx_start, count = np.unique(X, return_counts=True, return_index=True)
  X_vals = [dtype(x) for x in X_vals.tolist()]
  Y_vals = [r(Y[np.s_[s:s+c]]) for s,c in zip(idx_start, count)]
  return X_vals, Y_vals

def sort_by_x(X, Y):
  return [x for x, y in sorted(zip(X, Y), key=lambda p: p[0])], [y for x, y in sorted(zip(X, Y), key=lambda p: p[0])]

def split_contr(x):
  try:
    xerr = unumpy.std_devs(x)
    if not xerr.any(): xerr = None
    x = unumpy.nominal_values(x)
  except:
    xerr = None
  return x, xerr

def plot_func(p, x, y, **kwargs):
  x, xerr = split_contr(x)
  y, yerr = split_contr(y)
  kwargs.setdefault("fmt", "x")
  p.errorbar(x, y, xerr=xerr, yerr=yerr, **kwargs);

def main():
  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
  args = parser.parse_args()
  logger = hp.setup_logger(args.verbosity)

  for plot in schema.validate(plots):
    if not plot.active:
      continue
    x, y = {}, {}
    data_extract = {}

    for i, line in enumerate(plot.lines):
      x[i], y[i] = [], []

      for file in iglob(line.glob):
        run = SimpleNamespace(**vars(line))
        keys = ["x", "time"] + list(run.extract.keys())
        for key in keys:
          if not key in data_extract:
            data_extract[key] = {}
          data_extract[key][i] = []

      tune = [False, False]
      logger.info(line.glob)
      for file in iglob(line.glob):
        run = SimpleNamespace(**vars(line))
        run.file = file

        x_, sx = try_extract("x", run.x, run)
        y_, sy = try_extract("time", run.time, run)
        #if (x_ := try_extract("x", run.x, run)) and (y_ := try_extract("time", run.time, run)):
        if sx and sy:
          x[i].append(x_)
          y[i].append(y_)
          tune[file_contains(run.file, "QUDA (rank=0): Tuned block")] = True

          info = "   "
          for k,v in [("x", run.x), ("time", run.time)] + list(run.extract.items()):
            info += f"{k} = {try_extract(k, v, run)[0]}\t"
            data_extract[k][i].append(try_extract(k, v, run)[0])
          logger.debug(info)

      if tune[0] and tune[1]:
        logger.warning(f"Log files contain tuning and non-tuning sets: {list(iglob(line.glob))}")

      x[i], y[i] = sort_by_x(x[i], y[i])
      try:
        x[i], y[i] = merge_by_x(x[i], y[i], run.reduce)
        dtype = np.float64
      except:
        try:
          x[i], y[i] = merge_by_x(x[i], y[i], None, str)
        except:
          x[i], y[i] = merge_by_x(x[i], y[i], lambda a: str(set(map(str, a))), str)
        dtype = str

      for k,v in list(run.extract.items()) + [("time", run.time), ("x", x[i])]:
        z, data_extract[k][i] = sort_by_x(data_extract["x"][i], data_extract[k][i])
        try:
          _, data_extract[k][i] = merge_by_x(z, data_extract[k][i], run.reduce, dtype)
        except:
          _, data_extract[k][i] = merge_by_x(z, data_extract[k][i], lambda a: str(set(map(str, a))), dtype)

    properties = {"xlabel": "number of nodes"}
    plot.kwargs = {**properties, **plot.properties}

    with PdfPages(f"{plot.filename}") as pdf:
      logger.info(f"Creating file {plot.filename}")

      # just some printing
      for i, line in enumerate(plot.lines):
        d = {}
        for k,v in data_extract.items():
          if i in v:
            d[k] = v[i]
        try:
          Yi = data_extract['x'][i]/min(data_extract['x'][i])
          base_idx = np.argmin(data_extract["x"][i])
          Spi = get_speedup(data_extract["time"][i], base_idx=base_idx)
          d["scaling"] = Spi[line.restrict]
          if plot.scaling == 0: # strong
            d["efficiency"] = Spi[line.restrict]/Yi
          elif plot.scaling == 1: # weak
            d["efficiency"] = data_extract["time"][i][base_idx]/np.array(data_extract["time"][i])
        except:
          pass

        import pandas as pd
        df = pd.DataFrame(d)\
                .rename(columns={"time": "time [sec]"})
        # pd.set_option('display.max_colwidth', None)
        # table = df.to_latex(
        #   index=False,
        #   columns=["x", "nodeseconds"],
        #   formatters={
        #     "x": lambda x: int(unumpy.nominal_values(x)),
        #     "nodeseconds": lambda x: '{:.3uS}'.format(x),
        #   },
        # )
        table = df.to_string()
        logger.info(f"{plot.name}: {line.plot_args.get('label', 'unknown')}\n{table}")

      allx = reduce(lambda a, b: a+b, x.values())
      x_OK = bool([s for s in allx if isinstance(s, np.floating)])
      X = get_X(allx) if x_OK else list(allx)

      fig, axes = plt.subplots(1, 2 if x_OK else 1, layout='constrained', figsize=(10, 5))
      if not x_OK: axes = [axes]
      spd = 0 if x_OK else -1
      tts = 1 if x_OK else 0
      plt.suptitle(plot.name)

      kwargs = plot_args.get("ideal", {}).copy()
      if x_OK and plot.scaling == 1: plot_func(axes[spd], X, 1.0*np.ones_like(X), **kwargs) # weak
      for i, line in enumerate(plot.lines):

        # ideal scaling
        if x_OK and plot.scaling != 2:
          Xi = get_X(x[i], mx=int(np.log2(X[-1])))
          if plot.scaling == 0: # strong
            plot_func(axes[spd], Xi, get_speedup(1.0/Xi), **kwargs)

          if line.ideal_speedup:
            idx = np.where(X == int(x[i][0]))[0][0]
            if plot.scaling == 0: # strong
              plot_func(axes[tts], X[idx:], unumpy.nominal_values(np.array(X[idx]*y[i][0]/X)[idx:]), **kwargs)
            elif plot.scaling == 1: # weak
              plot_func(axes[tts], X[idx:], unumpy.nominal_values(y[i][0]*np.ones_like(X[idx:])), **kwargs)

        # speedup, TTS
        if x_OK:
          plot_func(axes[spd], x[i][line.restrict], get_speedup(y[i])[line.restrict], **{**plot_args.get("speedup", {}), **line.plot_args})
          plot_func(axes[tts], x[i][line.restrict], y[i][line.restrict], **{**plot_args.get("tts", {}), **line.plot_args})
        else:
          plot_func(axes[tts], np.arange(len(y[i][line.restrict])), y[i][line.restrict], **{**plot_args.get("tts", {}), **line.plot_args})

        kwargs["label"] = None

      if x_OK: axes[spd].set_ylabel('speedup' if plot.scaling == 0 else 'efficiency')
      axes[tts].set_ylabel('average time [sec]')
      if plot.scaling == 0: axes[spd].set_yscale("log", base=2)
      if plot.scaling == 0: axes[tts].set_yscale("log", base=10)
      axes[spd].legend()
      axes[tts].legend()

      if x_OK and plot.scaling == 0:
        axes[spd].set_yticks(ticks=X, labels=X, rotation=0)

      for ax in axes:
        ax.set(**plot.kwargs)
        if x_OK:
          ax.set_xscale("log", base=2)
          ax.set_xticks(ticks=X, labels=X, rotation=0)
        else:
          ax.set_xticks(ticks=np.arange(len(X)), labels=X, rotation=-90)

      logger.info(f"Plotting scaling and TTS ...")
      Path(os.path.dirname(plot.filename)).mkdir(parents=True, exist_ok=True)
      pdf.savefig(bbox_inches="tight")

      for key, value in data_extract.items():
        if key == "x" or key == "time": continue

        try:
          fig = plt.figure(figsize=(10, 5)) # A4
          for i, line in enumerate(plot.lines):
            if i in data_extract['x'] and i in value:
              xx, yy = sort_by_x(data_extract['x'][i], value[i])
              plot_func(plt, xx, yy, **line.plot_args)
          plt.title(key)
          plt.ylabel(key)
          plt.legend()
          plt.grid(True)
          if plot.scaling == 2:
            plt.xticks(ticks=unumpy.nominal_values(xx), labels=xx, rotation=-90)
          elif x_OK:
            plt.xscale("log", base=2)
            plt.xticks(ticks=X, labels=X, rotation=0)
          else:
            plt.xticks(ticks=np.arange(len(xx)), labels=xx, rotation=-90)

          for ax in fig.axes:
            ax.set(**plot.kwargs)

          print(colored(f"Plotting {key} ...", "green"))

          pdf.savefig(bbox_inches="tight")
          plt.close()
        except Exception as e:
          logger.error(e)
          raise


if __name__ == '__main__':
  main()
