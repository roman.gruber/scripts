#!/usr/bin/env python3
"""
Check sets of output files.
"""

import argparse
import numpy as np
import helpers as hp
import os
import ast
from termcolor import colored

def main():

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-v", "--verbose", action="count", help="increase output verbosity", default=0)
    required = parser.add_argument_group('required named arguments')
    required.add_argument('-i', '--infiles', help='input file(s) glob.', metavar='FILE', required=True)
    required.add_argument('-n', '--files-per-config', help='target number of sources.', type=int, required=True)
    required.add_argument('--nconfigs', help='target number of configs.', type=int, required=False)

    args = parser.parse_args()
    glob = "*" in args.infiles
    infiles, nsrc, nconfigs, run_name = hp.group_by_configs(hp.braced_glob(args.infiles, glob=glob), invasive=False)
    i = 0
    m = 0
    for k,v in infiles.items():
        size = []
        inexistent_files = False
        n = 0
        for infile in v:
            if os.path.isfile(infile):
                size.append(os.path.getsize(infile))
                n += 1
                m += 1
            else:
                inexistent_files = True

        string = [f"config {k} ({n} files)"]

        error = False
        if inexistent_files:
            error = True
            string.append(colored(f"some files don't exist", "red"))
        if not all(x == size[0] for x in size):
            string.append(colored(f"not all files have the same size", "red"))
            error = True
        if n > args.files_per_config:
            string.append(colored(f"too many files", "yellow"))
            error = True
        elif n < args.files_per_config:
            string.append(colored(f"missing files", "red"))
            error = True


        if not error:
            string.append(colored(f"all fine", "green"))
        else:
            i += 1

        if args.verbose or error:
            print(i, ", ".join(string))

    if args.nconfigs:
        print(f"total #files: {m}/{args.nconfigs*args.files_per_config} ({100*m/(args.nconfigs*args.files_per_config):.0f}%, {nconfigs}/{args.nconfigs} configs)")
    else:
        print(f"total #files: {m}/{nconfigs*args.files_per_config} ({100*m/(nconfigs*args.files_per_config):.0f}%, {nconfigs} configs)")

if __name__ == '__main__':
    main()
