#! /usr/bin/env python3
"""
Generate plots from certain metrics given in the input file.
"""


import os
import re
import argparse
import importlib.util
import itertools
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import helpers as hp
from glob import iglob
from types import SimpleNamespace
from schema import Schema, And, Or, Use, Optional, SchemaError, Const
from matplotlib.backends.backend_pdf import PdfPages
from uncertainties import ufloat, unumpy, UFloat
import funcy as fc
from scipy.optimize import curve_fit
import inspect
import pprint


logger = hp.get_logger()


# schema to validate input list above and convert to a SimpleNamespace
ns = lambda x: SimpleNamespace(**x)
schema = Schema([{
  Optional("active", default=False): bool,
  Optional("filename", default=None): And(str),
  Optional("name", default="unnamed"): str,
  Optional("properties", default={}): {str: Or(int, str)},
  Optional("sort_x", default=lambda: sorted): callable,
  "lines": [{
    Optional("active", default=True): bool,
    "glob": And(str, len, lambda g: next(iglob(g))),
    Optional("errors", default="raise"): And(str, lambda s: s in ("raise", "ignore")),
    "x": [callable],
    "y": [callable],
    Optional("plot_args", default={}): {str: Or(int, str)},
    Optional("model", default=None): {
      "function": callable,
      Optional("plot_args", default={}): {str: Or(int, str)},
    },
  }]
}])


def default(x):
  return x[0] if type(x) == list and len(x) == 1 else x

class ExtractException(Exception):
    pass

def extract(file, pattern, group, mapping = lambda x: x, grp_reduce = default, file_reduce = lambda x: x):
  """
  Extract data from a text file
  
  :param      file:              The log file
  :type       file:              string
  :param      pattern:           regex pattern to search the file
  :type       pattern:           string
  :param      group:             Pattern matching group to extract
  :type       group:             int or tuple of ints
  :param      mapping:           Mapping function to postprocess matches
  :type       mapping:           callable
  :param      grp_reduce:        Reduction function if multiple matches found
  :type       grp_reduce:        callable
  :param      file_reduce:       Reduction function over the file
  :type       file_reduce:       callable
  
  :returns:   Reduced mapped match
  :rtype:     mixed
  
  :raises     ExtractException:  If match not found
  """
  ret = []
  r = re.compile(pattern)
  with open(file, "r", encoding='utf-8', errors='ignore') as fp:
    lines = '\n'.join(fp.readlines())
    for result in r.finditer(lines):
      if type(group)==tuple:
        ret.append(list(result.group(*group)))
      else:
        ret.append([result.group(group)])

  hp.error(not ret, f"Unable to extract something in file {file} using pattern {pattern}", ErrorType=ExtractException)

  logger.debug(f"\nfrom {file}:")
  logger.debug(f"  extracted: {ret} using \"{pattern}\"")
  ret = [[mapping(y) for y in x] for x in ret]
  logger.debug(f"  mapped: {ret} using {mapping.__name__}()")
  ret = [grp_reduce(r) for r in ret]
  logger.debug(f"  group reduction: {ret} using {grp_reduce.__name__}()")
  ret = file_reduce(ret)
  logger.debug(f"  final reduction: {ret} using {file_reduce.__name__}()")
  return ret


def load_from_py_file(infile):
  spec = importlib.util.spec_from_file_location("infile", infile)
  module = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(module)
  return module

def mean_sem(arr, **kwargs):
  return ufloat(np.mean(arr, **kwargs), stats.sem(arr, **kwargs))

def ignore_errors(func):
  def wrapper(*args, **kwargs):
    try:
      return func(*args, **kwargs)
    except Exception as e:
      logger.debug(f"An error occurred, returning default value")
      return []
  return wrapper

def ufloat_default_format(u, format_spec):
    if not format_spec: format_spec = ".8e"
    return f"{format(u.nominal_value, format_spec)} +/- {format(u.std_dev, format_spec)}"
UFloat.__format__ = ufloat_default_format

def pp_dict(dict, _print=logger.debug):
  len_max = max([len(repr(key)) for key in dict.keys()])+2
  for key, value in dict.items():
    _print(f"{key:<{len_max}} {value}")

# like map and starmap, but the arguments of the function is **x
def starstarmap(func, iterable):
  return map(lambda x: func(**x), iterable)

def main():
  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument("-v", "--verbosity", action="count", help="increase output verbosity", default=0)
  parser.add_argument('-i', '--infile', required=True,
    help='Path to setup file that contains a dictionary validatable by the schema describing metrics for plots.')

  args = parser.parse_args()
  logger = hp.setup_logger()
  plots = schema.validate(load_from_py_file(args.infile).plots)
  wrap = {
    'ignore': ignore_errors,
    'raise': lambda x: x
  }

  active = lambda d: d.get("active", True)

  for plot in starstarmap(SimpleNamespace, filter(active, plots)):

    with hp.Pdf(f"{plot.filename}") as pdf:
      logger.info(f"Creating file {plot.filename}")

      fig, ax = plt.subplots(1, 1)
      plt.suptitle(plot.name)

      for line in starstarmap(SimpleNamespace, filter(active, plot.lines)):
        Xl, Yl = [], []
        for file in iglob(line.glob):
          line.file = file
          Xf, Yf = [], []

          for callable_x in map(wrap[line.errors], line.x):
            Xf.extend(callable_x(line))
          for callable_y in map(wrap[line.errors], line.y):
            Yf.extend(callable_y(line))

          hp.error(len(Xf)!=len(Yf), f"x and y don't have the same length {len(Xf) = }, {len(Yf) = }, {Xf = }, {Yf = }")
          Xl.extend(Xf)
          Yl.extend(Yf)

        data = {}
        for x, y in plot.sort_x(zip(Xl, Yl)):
          if x in data:
            data[x].append(y)
          else:
            data[x] = [y]

        pp_dict(data)
        data = fc.walk_values(mean_sem, data)
        pp_dict(data)

        X = np.array([x for x in data.keys()])
        Y = np.array([x.n for x in data.values()])
        yerr = np.array([x.s for x in data.values()])
        #hp.plot(plt, Y, X, yerr=yerr, **line.plot_args)
        plt.errorbar(X, Y, yerr=yerr, **line.plot_args)

        if line.model:
          popt, pcov = curve_fit(line.model.get("function"), X, Y)
          #popt, pcov = curve_fit(line.model.get("function"), X, Y, sigma=yerr, absolute_sigma=True)
          fit = lambda x: line.model.get("function")(x, *popt)
          solution = dict(zip(inspect.getfullargspec(line.model.get("function")).args[1:], popt))
          logger.debug(f"{solution = }")
          hp.plot(plt, fit(X), X, **line.model.get("plot_args"))

      ax.set(**plot.properties)
      plt.legend()
      pdf.savefig(bbox_inches="tight")


if __name__ == '__main__':
  main()
