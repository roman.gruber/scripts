#!/usr/bin/env python3
"""
Some statistical helper functions.
"""


import os
import logging
import itertools
import numpy as np
from multiprocessing import Pool
from uncertainties import ufloat, unumpy
from scripts import helpers as hp


logger = logging.getLogger("utils")


def error(test, msg, ErrorType=ValueError):
    if test:
        logger.critical(msg)
        raise ErrorType(msg)

def gauge_variance(G, equation=13):
    """
    Estimate the gauge variance

    :param      G:    Array with shape (nconfigs, ntime, nsrc, N0), where
                      nconfigs is the number of configs, ntime is the number of
                      time slices with sources, nsrc is the number of sources
                      per time slice, and N0 is the time extent of the lattice.
    :type       G:    numpy.ndarray

    :returns:   Gauge variance with shape (N0)
    :rtype:     numpy.ndarray
    """

    error(G.ndim != 4, f"G must have shape = (nconfigs, ntime, nsrc, N0), ({G.shape = })")

    # equation 13
    if equation==13:
        (nconfigs, ntime, nsrc, N0) = G.shape
        dtype = np.float128
        sigma = np.zeros(N0, dtype=np.dtype(dtype, metadata={"N0": N0}))

        meanG = np.mean(G, axis=0)

        if (nsrc==1):
            prefactor = (1.0/nsrc)
        else:
            prefactor = (1.0/nsrc - 1.0/(nsrc-1))
            sigma -= np.einsum("ctsn,ctsn->n", G, G, dtype=dtype)/((nsrc-1)*nconfigs)
            sigma += np.einsum(" tsn, tsn->n", meanG, meanG, dtype=dtype)/(nsrc-1)

        sigma += np.einsum("ctsn,cTSn->n", G, G, dtype=dtype)/(nsrc*nconfigs)
        sigma -= np.einsum("ctsn,ctSn->n", G, G, dtype=dtype)*prefactor/nconfigs
        sigma -= np.einsum(" tsn, TSn->n", meanG, meanG, dtype=dtype)/nsrc
        sigma += np.einsum(" tsn, tSn->n", meanG, meanG, dtype=dtype)*prefactor

        return sigma/(ntime**2*nsrc)

    # equation 10
    elif equation==10:
        (nconfigs, ntime, nsrc, N0) = G.shape
        sigma = np.zeros(N0, dtype=np.dtype(np.float64, metadata={"N0": N0}))
        for x0, y0 in itertools.product(range(ntime), repeat=2): # sum on x0,y0
            for i, j in itertools.permutations(range(nsrc), 2): # sum on i!=j
                sigma += (np.mean(G[:,x0,i]*G[:,y0,j], axis=0) - np.mean(G[:,x0,i], axis=0)*np.mean(G[:,y0,j], axis=0))

        return sigma/(ntime**2*nsrc*(nsrc-1))

    # old version
    else:
        G = G.reshape(G.shape[0], -1, G.shape[-1])
        N0, nsrc = G.shape[-1], G.shape[1]
        length = int((nsrc*(nsrc-1))/2)
        sigma = np.zeros(N0, dtype=np.dtype(np.float64, metadata={"N0": G.shape[-1]}))
        for i, j in itertools.combinations(range(nsrc), 2): # combines two *different* sources, but not necessarily two different time slices
            sigma += (np.mean(G[:,i]*G[:,j], axis=0) - np.mean(G[:,i], axis=0)*np.mean(G[:,j], axis=0))
        return sigma/length


def gauge_variance_old(G, equation=13):
    """
    Estimate the gauge variance (Tim still has to check) slow version!

    :param      G:    Array with shape (nconfigs, ntime, nsrc, N0), where
                      nconfigs is the number of configs, ntime is the number of
                      time slices with sources, nsrc is the number of sources
                      per time slice, and N0 is the time extent of the lattice.
    :type       G:    numpy.ndarray

    :returns:   Gauge variance with shape (N0)
    :rtype:     numpy.ndarray
    """

    error(G.ndim != 4, f"G must have shape = (nconfigs, ntime, nsrc, N0), ({G.shape = })")

    delta = lambda x, y: 1.0 if x==y else 0.0

    # equation 13
    if equation==13:
        (nconfigs, ntime, nsrc, N0) = G.shape
        sigma = np.zeros(N0, dtype=np.dtype(np.float64, metadata={"N0": N0}))
        for x0 in range(ntime): # sum on x0
            for i, j in itertools.permutations(range(nsrc), 2): # sum on i!=j
                sigma += (1.0/(nsrc-1))*(np.mean(G[:,x0,i]*G[:,x0,j], axis=0) - np.mean(G[:,x0,i], axis=0)*np.mean(G[:,x0,j], axis=0))
            for y0 in range(ntime): # sum on y0
                for i, j in itertools.product(range(nsrc), repeat=2): # sum on i,j
                    sigma += ((1.0 - delta(x0,y0))/nsrc)*(np.mean(G[:,x0,i]*G[:,y0,j], axis=0) - np.mean(G[:,x0,i], axis=0)*np.mean(G[:,y0,j], axis=0))

        return sigma/(ntime**2*nsrc)

    # equation 10
    elif equation==10:
        (nconfigs, ntime, nsrc, N0) = G.shape
        sigma = np.zeros(N0, dtype=np.dtype(np.float64, metadata={"N0": N0}))
        for x0, y0 in itertools.product(range(ntime), repeat=2): # sum on x0,y0
            for i, j in itertools.permutations(range(nsrc), 2): # sum on i!=j
                sigma += (np.mean(G[:,x0,i]*G[:,y0,j], axis=0) - np.mean(G[:,x0,i], axis=0)*np.mean(G[:,y0,j], axis=0))

        return sigma/(ntime**2*nsrc*(nsrc-1))

    # old version
    else:
        G = G.reshape(G.shape[0], -1, G.shape[-1])
        N0, nsrc = G.shape[-1], G.shape[1]
        length = int((nsrc*(nsrc-1))/2)
        sigma = np.zeros(N0, dtype=np.dtype(np.float64, metadata={"N0": G.shape[-1]}))
        for i, j in itertools.combinations(range(nsrc), 2): # combines two *different* sources, but not necessarily two different time slices
            sigma += (np.mean(G[:,i]*G[:,j], axis=0) - np.mean(G[:,i], axis=0)*np.mean(G[:,j], axis=0))
        return sigma/length


def total_variance(G):
    """
    Estimate the variance (gauge + noise source variance) of the correlator. Use this!

    :param      G:    Array with shape (nconfigs, nsets, nsrc, N0)
    :type       G:    numpy.ndarray

    :returns:   Total variance with shape (N0)
    :rtype:     numpy.ndarray
    """
    error(G.ndim != 4, f"G must have shape = (nconfigs, nsets, nsrc, N0), ({G.shape = })")
    Ghat = np.mean(np.real(G), axis=2) # average the sources -> new shape = (nconfigs, nsets, N0)

    # @choose
    #mean = lambda g: np.mean(g, axis=0)
    #sigma = unumpy.std_devs(gn.jackknife(Ghat, mean, parallel=False))**2   # option 1.1: variance wrt nconfigs -> new shape = (nsets, N0), using jackknife
    sigma = np.mean(Ghat**2, axis=0) - np.mean(Ghat, axis=0)**2             # option 1.2: variance wrt nconfigs -> new shape = (nsets, N0), explicitely
    sigma = np.mean(sigma, axis=0)                                          # option 1: average the sets -> new shape = (N0)
    #sigma = np.mean(Ghat**2, axis=(0,1)) - np.mean(Ghat, axis=(0,1))**2    # option 2: variance wrt (nconfigs, nsets) -> new shape = (N0)
    return sigma


class Functor:
    """
    Simple class that can be applied, with internal state (kwargs)
    """
    def __init__(self, f, **kwargs):
        self.f = f
        self.kwargs = {} if kwargs is None else kwargs
    def __call__(self, x):
        return self.f(x, **self.kwargs)


def eval_nominal_values(fn, x):
    return unumpy.nominal_values(fn(x))

@hp.cache_to_disk(arguments={"data": hp.hash_numpy_array, "fn": lambda f: f.__name__})
def jackknife(data, fn, axis=0, bar=range, parallel=True):
    """
    Apply the jackknife resampling method to a data set along an axis.

    :param      data:      Input data
    :type       data:      ndarray
    :param      fn:        Function to estimate the mean and variance of, must be picklable if parallel=True
    :type       fn:        callable
    :param      axis:      The axis in data over which to jackknife
    :type       axis:      int
    :param      bar:       A potential progress bar
    :type       bar:       callable that returns an iterable
    :param      parallel:  Use multiprocessing to parallelize
    :type       parallel:  bool

    :returns:   Result as a numpy array of ufloats
    :rtype:     uarray (numpy array of dtype=ufloats)
    """
    n = data.shape[axis]
    hp.error(n==1, f"Cannot do jacknife when axis has length is 1 {data.shape = }, {axis = }, {fn.__name__ = }.")

    data_inputs = map(lambda k: np.array(np.delete(data, k, axis=axis), dtype=data.dtype), bar(n))
    mapper = Pool(os.cpu_count()).starmap if parallel else itertools.starmap
    r = np.array(list(mapper(eval_nominal_values, ((fn, e) for e in data_inputs))))

    j_est = np.mean(unumpy.nominal_values(r), axis=0)
    bias = (n-1)*(eval_nominal_values(fn, data) - j_est)
    variance = unumpy.nominal_values(((n-1)/n)*sum((r[k] - j_est)**2 for k in range(n)))
    error = np.sqrt(variance)
    return unumpy.uarray(j_est, error)
