# Scripts

## submit.py

Script to create separate builds of openqcd for strong or weak scaling or just runs with different compile settings, input files, etc ... This script produces a bunch of binaries, input files, and sbatch files based on given input templates. The sbatch files have to be executed afterwards manually. 

Uses python3 features like the walrus operator, so don't forget to:

```bash
module load cray-python
```

