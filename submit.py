#! /usr/bin/env python3
"""
Script to create separate builds of openqcd for strong or weak scaling or just
runs with different compile settings, input files, ... This script produces a
bunch of binaries, input files, and sbatch files based on given input templates.
The sbatch files have to be executed afterward manually.

Uses python3 features like the walrus operator, so don't forget to:
module load cray-python
"""

from pathlib import Path
import numpy as np
import itertools
import os
import re
import subprocess
import shutil
import math
from types import SimpleNamespace
from schema import Schema, And, Or, Use, Optional, SchemaError, Literal
import tempfile
import filecmp
import importlib.util
import argparse
import ast
import traceback
import helpers as hp

logger = hp.get_logger()

class _dict(dict):
  read_elements = set()
  def __getitem__(self, k):
    s = super(_dict, self)
    return s.__getitem__(k if s.__contains__(k) else f'!{k}')

  def __contains__(self, k):
    s = super(_dict, self)
    return s.__contains__(k) or s.__contains__(f'!{k}')

  def get(self, k, default=None):
    self.read_elements.add(k)
    return self[k] if k in self else default

# schema to validate input list above and convert to a SimpleNamespace
Ns = lambda x: SimpleNamespace(**x)
schema = Schema(And({
  "work_dir": And(str, len),
  Optional("substitute", default={}): {str: Or(int, str, callable)},
  "templates": [And({
    "file": And(str, os.path.exists),
    "template": And(str, os.path.exists),
    "bak": And(str, len),
  }, Use(Ns))],
  Optional("checks", default=[]): Optional([{
    "test": callable,
    "fail": callable,
  }, Use(Ns)]),
  Optional("env", default={}): {str: Or(int, str, callable)},
  "runs": [And({
    "run_name": And(str, len),
    "lattice_size": And(Use(np.array), lambda n: len(n)==4 and n.dtype==int),
    Optional("weak", default=False): bool,
    Optional("input_file_template", default=None): And(str, os.path.exists),
    "sbatch_template": And(str, os.path.exists),
    Optional("job_name", default="{{run_name}}/{{binary}}/{{setup_str}}"): And(str, len),
    "setups": And(Use(lambda x: np.array(x, dtype=dict)), len),
    "progs": And([str], len),
    "substitute": {str: Or(int, str, callable)},
    Optional("env", default={}): {str: Or(int, str, callable)},
    Optional("cleanup", default=False): bool,
    Optional("cmd", default="sbatch {{sbfile}}"): str,
    Optional("build_cmd", default="make {{binary}}"): str,
  }, Use(Ns))]
}, Use(Ns)))


def extract_from_ini_file(file, pattern, dtype):
  p = re.compile(pattern)
  with open(file, "r") as fp:
    for line in fp:
      if (result := p.search(line)):
        return dtype(result.group(1))
  return None

def create_dirs(dirs):
  for dir in dirs:
    Path(dir).mkdir(parents=True, exist_ok=True)

def str_replace(string, replacements, times=3):
  p = re.compile(r"\{\{([\w\d\:]+)\}\}")
  replaced_items = []
  for result in p.finditer(string):
    if result.group(1) in replacements:
      replaced_items.append(result.group(1))
      string = string.replace(result.group(0), str(replacements[result.group(1)]))

  if times > 1:
    string, items = str_replace(string, replacements, times-1)
    replaced_items.extend(items)
  return string, replaced_items

def file_replace(file, replacements):
  replaced_items = []
  with open(file, "r+") as fp:
    lines = fp.readlines()
    for i, line in enumerate(lines):
      lines[i], items = str_replace(line, replacements)
      replaced_items.extend(items)
    fp.truncate(0)
    fp.seek(0)
    fp.writelines(lines)
    return replaced_items

def regex_in_file(file, pattern, replacement):
  p = re.compile(pattern)
  with open(file, "r+") as fp:
    lines = fp.readlines()
    for i, line in enumerate(lines):
      if (result := p.search(line)):
        lines[i] = replacement + '\n'
    fp.truncate(0)
    fp.seek(0)
    fp.writelines(lines)

def check_str_for_placeholders(str, var_name):
  error(ph := re.findall(r"\{\{[\w\d\:]+\}\}", str),
    f"There are still unfilled placeholders {','.join(set(ph))} in string {var_name}: {str}", stop=False)

def check_file_for_placeholders(fname, actual_fname=None):
  with open(fname) as f:
    error(ph := re.findall(r"\{\{[\w\d\:]+\}\}", f.read()),
      f"There are still unfilled placeholders in the file {fname if actual_fname is None else actual_fname}: {', '.join(set(ph))}", stop=False)

def adjust_template(d, orig_file, template):
  temp = tempfile.NamedTemporaryFile()
  shutil.copy(template, temp.name)
  replaced_items = file_replace(temp.name, d)
  check_file_for_placeholders(temp.name, template)
  if not filecmp.cmp(orig_file, temp.name):
    shutil.copy(temp.name, orig_file)
  return replaced_items

def stringify(x, sep="_", kv_sep="", suppress="!"):
  if isinstance(x, dict):
    return sep.join(f"{stringify(k)}{kv_sep}{stringify(v)}" for k,v in x.items() if not k.startswith(suppress))
  elif isinstance(x, list):
    return "x".join(map(stringify, x))
  elif x is None:
    return ""
  else:
    return str(x).replace(" ", "")

def env_value(v):
  return f'"{v}"' if isinstance(v, str) else v

def env_stringify(env):
  env_str = ""
  for variable, value in env.items():
    env_str += f"export {variable}={env_value(value)}{os.linesep}"
  return env_str

def build(binary, dir, clean=True, silent=True, build_cmd="make {{binary}}"):
  env = {
    "stdout": subprocess.DEVNULL if silent else None,
    "cwd": dir,
  }
  if clean:
    p = subprocess.Popen(["make", "clean"], **env)
    p.wait()
  build_cmd, replaced_items = str_replace(build_cmd, {"binary": binary})
  check_str_for_placeholders(build_cmd, "build_cmd")
  p = subprocess.Popen(build_cmd.split(), **env)
  exit_code = p.wait()
  error(exit_code != 0, f"Build command \"{build_cmd}\" failed: with exit code {exit_code}")
  return replaced_items

def key(k, suppress="!"):
  return k[1:] if k.startswith(suppress) else k

def val(v):
  return v

def check_unique(x):
  return len(x) == len(set(x))

def error(test, msg, stop=True, exit_code=1):
  if test:
    logger.error(msg)
    if stop: exit(exit_code)

def main():
  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument('-i', '--infile', required=True,
    help='Path to setup file that contains a dictionary validatable by the schema.')
  parser.add_argument('--dry-run', help="Skip all changes made to files, including building (i.e. just print the command(s))", default=False, action="store_true")
  args = parser.parse_args()
  logger = hp.setup_logger()

  spec = importlib.util.spec_from_file_location("infile", args.infile)
  infile = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(infile)
  s = schema.validate(infile.setup)
  invasive = not args.dry_run

  if invasive:
    for t in s.templates:
      shutil.copy(t.file, t.bak)
    create_dirs([s.work_dir])

  cmds = []
  for orun in s.runs:
    dir = os.path.abspath(f"{s.work_dir}/{orun.run_name}")
    if invasive and orun.cleanup and os.path.isdir(dir):
      answer = input(f"Sure to remove {dir}? [y,n]: ").lower()
      shutil.rmtree(dir) if answer in ["y","yes"] else exit()

    setups = [(prog, stringify(setup)) for i, (prog, setup) in enumerate(itertools.product(orun.progs, orun.setups))]
    error(not check_unique(setups), f"Not all setup strings are uniqe:\n{setups}")

    for i, (prog, setup) in enumerate(itertools.product(orun.progs, orun.setups)):
      run = SimpleNamespace(**vars(orun))

      run.idx = i
      run.setup = _dict(setup.copy())
      run.run_dir_rel = run.run_name
      run.run_dir = os.path.abspath(f"{s.work_dir}/{run.run_dir_rel}")

      run.prog = prog

      if run.weak:
        run.lattice_size = np.array(np.array(run.setup.get("NPROC"))*run.setup.get("L"), dtype=int)
      else:
        run.setup.setdefault("L", list(np.array(run.lattice_size/run.setup.get("NPROC"), dtype=int)))

      if np.prod(run.lattice_size) != 0 and not np.array_equal(np.array(run.setup.get("NPROC")*np.array(run.setup.get("L"))), np.array(run.lattice_size)):
        error(True, f"NPROC*L != N")

      # Expand key=[1,2] into key0=1, key1=2
      for k, v in run.setup.copy().items():
        if isinstance(v, list):
          for j, vv in enumerate(run.setup.get(k)):
            run.setup[f"{k}{j}"] = vv

      dir = os.path.abspath(os.path.dirname(run.prog))
      run.binary = os.path.basename(run.prog)
      run.nproc_str = "x".join(map(str, list(run.setup.get("NPROC"))))
      run.setup_str = stringify(setup)
      run.L_str = "x".join(map(str, list(run.setup.get("L"))))
      run.N_str = "x".join(map(str, list(run.lattice_size)))
      run.job_dir_rel = f"{run.run_dir_rel}/{run.binary}_{run.setup_str}"
      run.job_dir =  os.path.abspath(f"{s.work_dir}/{run.job_dir_rel}")
      if invasive:
        create_dirs([run.job_dir])

      run.infile = os.path.abspath(f"{run.job_dir}/{run.binary}.in")
      run.executable = os.path.abspath(f"{run.job_dir}/{run.binary}")
      run.sbfile = os.path.abspath(f"{run.run_dir}/sbatch_{run.binary}_{run.setup_str}.sh")
      run.ranks = np.prod(run.setup.get("NPROC")) # number of processes
      run.work_dir = os.path.abspath(s.work_dir)

      user_substitutes = set(s.substitute.keys())
      user_substitutes.update(run.substitute.keys())
      user_substitutes.update(run.setup.keys())
      user_substitutes = list(map(key, user_substitutes))

      run.substitute = {**s.substitute, **run.substitute}
      run.substitute = {**vars(run), **run.substitute}
      run.substitute = {k: v(run) if callable(v) else v for k, v in run.substitute.items()}

      run.env = {k: v(run) if callable(v) else v for k, v in itertools.chain(s.env.items(), run.env.items())}
      run.env_str = env_stringify(run.env)

      # postprocessing of keys and values of the substitution dict
      sub = {key(k): val(v) for k, v in {**run.substitute, **run.setup}.items()}

      for check in s.checks:
        error(not check.get('test')(Ns(run.substitute)), f"Check failed: {check.get('fail')(Ns(run.substitute))}")

      replaced_items = []
      if invasive:
        for t in s.templates:
          replaced_items.extend(adjust_template(sub, t.file, t.template))

      run.sbatch_template, items = str_replace(run.sbatch_template, sub)
      replaced_items.extend(items)
      check_str_for_placeholders(run.sbatch_template, "sbatch_template")
      if invasive:
        shutil.copy(run.sbatch_template, run.sbfile)

      if run.input_file_template:
        run.input_file_template, items = str_replace(run.input_file_template, sub)
        replaced_items.extend(items)
        check_str_for_placeholders(run.input_file_template, "input_file_template")
        if invasive:
          shutil.copy(run.input_file_template, run.infile)
        run.log_dir = extract_from_ini_file(run.infile, r"log_dir\s+([^\s]+)", str)
        if run.log_dir:
          run.log_dir = os.path.abspath(run.log_dir)
          if invasive:
            Path(run.log_dir).mkdir(parents=True, exist_ok=True)
      if invasive:
        if run.input_file_template:
          replaced_items.extend(file_replace(run.infile, sub))
          check_file_for_placeholders(run.infile, run.input_file_template)
        replaced_items.extend(file_replace(run.sbfile, {**sub, **{"env_str": run.env_str}}))
        check_file_for_placeholders(run.sbfile, run.sbatch_template)

      if invasive:
        add_info = f"N={run.N_str}" if run.weak else f"L={run.L_str}"
        logger.info(f"# Building {run.binary} for {run.run_name} (#{run.idx}, {run.setup_str}, {add_info}) {i+1}/{len(run.progs)*len(run.setups)} ...")
        replaced_items.extend(build(run.binary, dir, clean=False, silent=True, build_cmd=run.build_cmd))
        shutil.copy(os.path.abspath(run.prog), run.executable)

      run.cmd, items = str_replace(run.cmd, sub)
      replaced_items.extend(items)
      check_str_for_placeholders(run.cmd, "cmd")
      cmds.append(run.cmd)

      replaced_items.extend(run.setup.read_elements)
      missing_keys = set(user_substitutes).difference(replaced_items)
      if missing_keys:
        logger.warning(f"Warning: There are keys never used: {missing_keys}")

  if invasive:
    for t in s.templates:
      shutil.copy(t.bak, t.file)
  cmd = '\n'.join(cmds)
  logger.info(f"# Call with:\n{cmd}")

if __name__ == '__main__':
  main()
